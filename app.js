const express = require("express");
const pug = require("pug"); 
const path = require("path");
const fs = require("fs");
const multer = require("multer");
const { ENGINE_METHOD_ALL } = require("constants");


const app = express();
//set storage engine

const storage = multer.diskStorage({
  destination: "./public/uploads/",
  filename: function (req, file, cb) {
    cb(
      null,
      file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    );
  }
});

//init upload

const upload = multer({
  storage: storage,
  limits: { fileSize: 3000000 },
  fileFilter: function (req, file, cb) {
    checkFileType(file, cb);
  },
}).single("uploadPhoto");

//check file type
function checkFileType(file, cb) {
  //allowed ext
  const filetypes = /jpeg|jpg|png|gif/;
  //check ext
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  //check mime
  const mimetype = filetypes.test(file.mimetype);

  if (mimetype && extname) {
    return cb(null, true);
  } else {
      cb("Error: Images Only")
  }
}
//MulterLesson
app.set("view engine", "pug" )

//public folder
app.use(express.static("./public/uploads"));



//set route to public folder
app.get("/", (req, res) => {
  const path = "./public/uploads";
    fs.readdir(path,function(err, items){
      const photos = items.map((item) =>{
        return item 
      })
      res.render("index", {photos:photos})
    })
    
  });


app.post("/upload", upload, (req, res, next) => {

  console.log("Upload:" + req.file.filename)
  // res.redirect(200, `upl/${req.file.filename}`)
  res.render("uploads", {image:req.file.filename})
next()
})


const port = 3000;
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
